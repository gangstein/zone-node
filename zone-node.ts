import { IMap, ICoordinates } from '@shared/types';

import { deliveryZonesListModel } from '@entities/delivery-zones/list';
import { IZoneCreate, IZoneNode } from '@entities/delivery-zones/list/model';

const { formatZonesForDraw, formatZonesForDownload, getColor, flatZones } = deliveryZonesListModel;

export class ZoneNode implements IZoneNode {
	public coordinates: ICoordinates = null;
	public name: string = null;
	public id: string | number = null;
	public sap_id: string = null;
	public parent: IZoneNode = null;
	public children: IZoneNode[] = [];
	public color: string = null;
	public selected = false;
	public expandable = true;
	public level: number = null;
	public type;
	private _map: IMap = null;
	private _selectedNodes: IZoneNode[] = [];

	get disabled() {
		const unlockedDepth = this.host.selectedNodes[0]?.depth ?? null;

		return unlockedDepth !== null && unlockedDepth !== this.depth;
	}

	get selectedNodes() {
		if (this.parent) {
			return this.parent.selectedNodes;
		}

		return this._selectedNodes;
	}

	set selectedNodes(nodes) {
		if (this.parent) {
			this.parent.selectedNodes = nodes;
		} else {
			this._selectedNodes = nodes;
			this.draw();
			this.map.polygon = null;
		}
	}

	get host() {
		if (this.parent) {
			return this.parent.host;
		}

		return this;
	}

	get map() {
		if (this.parent) {
			return this.parent.map;
		}

		return this._map;
	}

	set map(map: IMap) {
		if (this.parent) {
			this.parent.map = map;
		} else {
			this._map = map;
		}
	}

	get depth() {
		return this.parent ? this.parent.depth + 1 : 0;
	}

	constructor({
		coordinates = null,
		name = null,
		id = null,
		type = 'zone',
		sap_id = null,
		level = null,
	}: IZoneCreate) {
		this.coordinates = coordinates;
		this.name = name;
		this.id = id;
		this.level = level;
		this.sap_id = sap_id;
		this.type = type;
	}

	public draw() {
		this.map.drawZones(formatZonesForDraw(this.selectedNodes));
	}

	public setExpandable(expandable: boolean) {
		if (this.parent) {
			this.parent.expandable = expandable;
			this.parent.setExpandable(expandable);
		}
	}

	public addNode(node: IZoneNode) {
		node.parent = this;
		this.children.push(node);
	}

	public remove() {
		if (this.selected) {
			this.select();
		}

		this.children.forEach(child => child.remove());

		if (this.parent) {
			this.parent.children = this.parent.children.filter(child => child !== this);

			if (!this.parent.children.length) {
				this.parent.remove();
			}
		}
	}

	public setColor(color: string) {
		this.color = color;
		this.children.forEach(child => child.setColor(color));
	}

	public select(color?: string) {
		if (this.disabled) {
			return;
		}

		this.selected = !this.selected;

		if (this.selected) {
			this.setColor(color ?? getColor(this.selectedNodes));
			this.selectedNodes = [...this.selectedNodes, this];
		} else {
			this.setColor(null);
			this.selectedNodes = this.selectedNodes.filter(node => node !== this);
		}

		this.setExpandable(this.parent.children.every(child => !child.selected));
	}

	public updateAll(data: IZoneNode[]) {
		const selectedNodes = this.selectedNodes.map((node) => ({ color: node.color, name: node.name, id: node.id }));

		this.host.remove();

		data.forEach((zone) => {
			this.host.addNode(zone);
		});

		const flattenZones = flatZones(this.host.children);

		flattenZones.forEach((child) => {
			const compareChild = child.id ?? child.name;

			const currentNode = selectedNodes.find(node => {
				const compareNode = node.id ?? node.name;

				return compareNode === compareChild;
			});

			if (currentNode) {
				child.select(currentNode.color);
			}
		});
	}

	public formatForDownloading = () => {
		const getData = () => {
			if (this.selectedNodes.length) {
				return formatZonesForDraw(this.selectedNodes);
			}

			return this.host.children.flatMap((child) => formatZonesForDraw(child.children));
		};

		return formatZonesForDownload(getData());
	};

	static init(map: IMap, data?: IZoneNode[]) {
		const host = new ZoneNode({
			type: 'host',
			name: 'host',
		});

		host.map = map;
		data?.forEach((zone) => {
			host.addNode(zone);
		});

		return host;
	}
}
